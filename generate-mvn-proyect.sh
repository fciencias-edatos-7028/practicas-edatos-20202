#!/bin/bash

#######################################################################
#Script Name : generate-mvn-proyect.sh
#Description : Generates a maven proyect according to edatos.
#Args
#Author      : Isaac Salvador Hernández Pompa
#Email       : isaacsalvador18@ciencias.unam.mx
#######################################################################

mvn archetype:generate \
    -DgroupId=unam.fciencias.edatos \
    -DartifactId=p03 \
    -DarchetypeGroupId=org.apache.maven.archetypes \
    -DarchetypeArtifactId=maven-archetype-simple \
    -DarchetypeVersion=1.4 \
    -DinteractiveMode=false

