# Práctica 04
## Árboles y recorridos
El objetivo es:
* Implementar árboles binarios de búsqueda.
* Implementar recorridos en árboles.

## Descripción
Deberás implementar los árboles binarios de búsqueda para garantizar
la realización de los tres recorridos existentes:
* Preorder
* Inorder
* Postorder

Para garantizar que la implementación sea la adecuada, es necesario
escribir pruebas unitarias para:
* Inserción
* Búsqueda
* Recorridos
  * Preorder
  * Inorder
  * Postorder

## Ejemplos
Para los siguientes árboles los recorridos deberían dar:

```mermaid
graph TD;
D-->B
D-->F
B-->A
B-->C
F-->null
F-->G
```

Preorder:  D B A C F G

Inorder:   A B C D F G

Postorder: A C B G F D


```mermaid
graph TD;
P-->F
P-->S
F-->B
F-->H
H-->G
H-->null1
S-->R
S-->Y
Y-->T
Y-->Z
T-->null2
T-->W
```

Preorder:  P F B H G S R Y T W Z

Inorder:   B F G H P R S T W Y Z

Postorder: B G H F R W T Z Y S P

## Recuerda
* El nombre de la rama será: **práctica-04**
* Usar genéricos
* Seguir *checkstyle*
* Documentar debidamente el código
* Seguir la guía de estilo para commits
* Exponer motivos y consideraciones de diseño de tu programa en el README.

## Puntos extras
Cada inciso representa un punto extra sobre cualquiera de las prácticas
pasadas, siempre que se cumpla que toda la práctica sea entregada completa 
además de proveer un ejemplo no trivial que muestre el correcto funcionamiento
del inciso que se resuelve:

* Garantizar la unicidad de los elementos en el árbol al momento de insertar

* Implementar un método que regrese el elemento mínimo en este conjunto, mayor
o igual al elemento dado.

* Implementar un método que regrese el elemento más pequeño del conjunto.

* Si el total del acumulado de las observaciones de las prácticas pasadas
 no se encuentran aquí, también es un punto extra.



## Puntos a evaluar
Adicional a los puntos mencionados en las **Especificaciones 
generales de entrega de prácticas v.0.2**, se tomarán en cuenta:

* Seguir las indicaciones de entrega.
