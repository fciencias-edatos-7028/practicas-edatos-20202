# Práctica 02
## Calificando cuestionarios automáticamente
El objetivo es:
* Implementar el TDA **Lista Simplemente Ligada**.
* Aplicar la estructura de datos antes mencionada para la resolución de problemas.

## Problemática
Se requiere un programa que, dado un conjunto de preguntas y respuestas, se le presente al usuario de forma secuencial
para que este las responda y pueda ser evaluado en cultura general.

Para esto es necesario definir lo siguiente:
* El número de preguntas a responder.
* El número de intentos que tendrá para atinar la respuesta correcta.
* Para cada pregunta:
  * La categoría (p.ejem: Matemáticas, Historia, Rock & Roll, etc.)
  * La pregunta
  * El número de posibles respuestas (p. ejem: 2, 3, 4, 5, etc.)
  * Para cada respuesta:
    * La respuesta.
    
El programa nos deberá informar cuántas respuestas fueron asertadas y cuáles fueron erróneas, además deberá otorgar
el porcentaje respecto al total de preguntas *respondidas vs. acertadas*.

## Desarrollo
El desarrollo será en dos partes, la primera parte consistirá en:
* Implementar la estructura de datos **Lista Simplemente Ligada**, teniendo en cuenta las siguientes firmas de métodos:
  * `boolean add(E e)` agrega el elemento especificado al final de la lista.
  * `boolean contains(Object o)` `true` si la lista contiene el elemento especificado.
  * `E element()` obtiene pero no elimina la cabeza de la lista.
  * `E remove()` elimina la cabeza de la lista.
  * `int size()` el número de elementos en la lista.
* Cargar a la memoria del programa toda la información necesaria que necesaria para desplegar el cuestionario.

Para esta última parte es necesario guardar la información en un archivo para después cargarlo a la memoria 
principal del programa.

**Nota:** Si necesitas otros métodos que faciliten la implementación de la solución, puedes implementarlos, solo
asegúrate de escribir documentación de ellos y que sean compatibles con los "obligatorios" que se piden.

La segunda parte se encargará de desplegar el cuestionario para interactuar con el usuario. Puedes optar por usar
una extensión de la UI (Interfaz de Usuario, en inglés *User Interface*) de la práctica anterior.

Se recomienda para la primera parte, escribir pruebas unitarias. En caso de que optes por escribirlas, deberás informar
para darte más tiempo para su implementación. Si aceptas el reto, tendrás puntos extras en prácticas subsecuentes o 
en algún otro rubro de evaluación.

## Restricciones
Para leer y cargar la información del archivo puedes ocupar cualquier estructura de datos. Para interactuar y procesar
el cuestionario solo puedes hacer uso de tu implementación del TDA **Lista Simplemente Ligada**.


## Puntos a evaluar
Adicional a los puntos mencionados en las **Especificaciones generales de entrega de prácticas**, se tomarán en cuenta:
* Las razones en el diseño de la solución del problema a resolver:
  * Estructura de datos.
  * Clases para leer y procesar la información del archivo.
  * Clases de soporte para la aplicación.

* La correcta ejecución del programa para el *happy path*.
